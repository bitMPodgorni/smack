//
//  SocketService.swift
//  Smack
//
//  Created by Michał Podgórni on 30/11/2018.
//  Copyright © 2018 Michał Podgórni. All rights reserved.
//

import UIKit
import SocketIO

class SocketService: NSObject {
    
    static let instance = SocketService()
    var socket : SocketIOClient!
    let manager = SocketManager(socketURL: URL(string: BASE_URL)!)
    
    override init() {
        super.init()
        
        socket = manager.defaultSocket
    }
    
    func estabilishConnection() {
        socket.connect()
    }
    
    func closeConnection() {
        socket.disconnect()
    }
    
    func addChannel(channelName: String, channelDescription: String, completion: @escaping CompletionHandler) {
        socket.emit("newChannel", channelName, channelDescription)
        completion(true)
    }
    
    func getChannel(completion: @escaping CompletionHandler) {
        socket.on("channelCreated") { (dataArray, ack) in
            guard let channelName = dataArray[0] as? String else {return}
            guard let channelDesc = dataArray[1] as? String else {return}
            guard let channelID = dataArray[2] as? String else {return}
            
            let newChannel = Channel(_id: channelID, name: channelName, description: channelDesc, __v: nil)
            MessageService.instance.channels.append(newChannel)
            completion(true)
        }
    }
    
}
